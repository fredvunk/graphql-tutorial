# GraphQL tutorial

# PATH

set PATH=%PATH%;C:\Program Files\PostgreSQL\12\bin\

# Fixed Scripts

    "createdb": "cross-env psql -f ./database/create_db.sql postgres://localhost:5432/postgres",
    "createschema": "cross-env psql -f ./database/schema.sql postgres://localhost:5432/hackerbook",
    "loaddata": "cross-env psql -f ./database/load_data.sql postgres://localhost:5432/hackerbook",
    "dropdb": "cross-env psql -f ./database/drop_db.sql postgres://localhost:5432/postgres",
    "dropschema": "cross-env psql -f ./database/drop_schema.sql postgres://localhost:5432/hackerbook"

# Fix no user & Role

login as root user then :
create user "name" with password 'password';
alter user "name" createdb, Superuser;
\dg to see users

# Config change

pg_hba.conf Method = trust for All

# Queries

## CreateBook

mutation CreateBook($googleBookId: ID!){
  createBook(googleBookId: $googleBookId) {
id
title
}
}
query variables: {
"googleBookId": "T0L2nUOPUqAC"
}

## book data

query Book($id: ID!) {
  book(id: $id) {
id
title
description
imageUrl
rating
reviews {
id
rating
title
comment
user {
name
imageUrl
}
}
}
}
query variables : {
"id": 1
}

## Search db

query Search {
search(query: "peter") {
\_\_typename
... on Book {
id
title
}
... on Review {
id
rating
comment
}
... on User {
id
name
}
... on Author {
id
name
}
}
}
